package exp5_1;
import java.util.Stack;
import java.util.StringTokenizer;

public class MyBC{
    private Stack<Character> stack1;
    public char Precede(char a,char b)
    {
        if(a=='#')
            if(b!='#')
                return '<';
            else
                return '=';
        if(a==')')
            return '>';
        if(a=='(')
            if(b!=')')
                return '<';
            else
                return '=';
        if(a=='/'||a=='*')
            if(b!='(')
                return '>';
            else
                return '<';
        if(a=='-'||a=='+')
            if(b!='*'&&b!='/'&&b!='(')
                return '>';
            else
                return '<';
        return '>';
    }
    public MyBC() {
        stack1 = new Stack<Character>();
        stack1.push('#');
    }
    public String turn(String expr) {
        int  result = 0;
        String token;
        char topelem,optr;
        char[] exper1 = new char[100];
        int i = 0;

        StringTokenizer tokenizer = new StringTokenizer (expr);
          /*while (tokenizer.hasMoreTokens()) {
              System.out.println(tokenizer.nextToken());
          }*/
        while (tokenizer.hasMoreTokens())
        {
            token = tokenizer.nextToken();

            //如果是运算符，调用isOperator

            if (isOperator(token))
            {
                //调用Precede比较优先级

                topelem=stack1.peek();

                optr = token.charAt(0);

                if(Precede(topelem,optr)=='<')
                {

                    stack1.push(optr);

                }
                else if(Precede(topelem,optr)=='=')
                {
                      /*if(optr==')'){
                          optr=stack1.pop();
                          while(optr!='('){
                              exper1[i++]=optr;
                              exper1[i++]=' ';
                              optr=stack1.pop();
                          }
                      }*/
                    optr =stack1.pop();
                    exper1[i++] = optr;
                    exper1[i++] = ' ';
                }
                else if(Precede(topelem,optr)=='>')
                {
                    optr =stack1.pop();
                    //从运算符栈中退出栈顶元素并放入后缀表达式exper1
                    exper1[i++] = optr;
                    exper1[i++] = ' ';
                }
            }//如果是(则入栈
            else if(token.equals("(")) {
                optr = token.charAt(0);
                stack1.push(optr);
            }//如果是）则退栈直到出现第一个（
            else if(token.equals(")")) {
                optr = stack1.pop();
                while(optr!='(')
                {
                    exper1[i++] = optr;
                    exper1[i++] = ' ';
                    optr = stack1.pop();
                }
            }
            else//如果是操作数
            //操作数放入后缀表达式exper1
            {
                optr = token.charAt(0);
                //System.out.println(optr);
                exper1[i++]=optr;
                exper1[i++] = ' ';

            }
        }
        while(!stack1.isEmpty())
        {
            optr = stack1.pop();
            if(optr!='#'){
                exper1[i++] = optr;
                exper1[i++] = ' ';
            }
        }
        //System.out.println(exper1);

        return ToString(exper1);
    }
    //@Override
    private boolean isOperator(String token)
    {

        return (token.equals("+") || token.equals("-") ||token.equals("*") || token.equals("/") );
    }
    public  static String ToString(char[] exper1){
        int length = exper1.length;
        String str=" ";
        for(int i=0;i<length;i++)
        {
            str=str+exper1[i];
        }
        //System.out.println(str);
        return str;
    }
     /*public static void main(String[] args) {
         String pp=null;
         MyBC turner = new MyBC();

         pp=turner.turn("( 1 + 2 ) * ( ( 8 - 2 ) / ( 7 - 4     ) )");
         System.out.println(pp);
     }*/
}