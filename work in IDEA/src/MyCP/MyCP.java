package MyCP;
import java.io.*;
public class MyCP {
    public static void main(String[] args) {
        String choose = args[0];
        String File1 = args[1];
        String File2 = args[2];
        File sourceFile = new File(File1);
        File targetFile = new File(File2);
        int ReadLenth = 0;
        if (choose.equals("-tx")) {
            ReadLenth = 1;
        }
        else if (choose.equals("-xt")) {
            ReadLenth = 4;
        }
        else {
            System.out.println("输入参数错误！");
            System.exit(0);
        }
        char [] c= new char[ReadLenth];
        try{
            Writer out = new FileWriter(targetFile);
            Reader in = new FileReader(sourceFile);
            int n= -1;
            while ((n=in.read(c,0,ReadLenth))!=-1){
                String number="";
                if(ReadLenth==1){
                    number=Integer.toBinaryString((int)c[0]-48);
                    while (number.length()<4){
                        number="0"+number;
                    }
                    out.write(number);
                }
                if (ReadLenth==4){
                    for (int i=0;i<n;i++){
                        number=number+c[i];
                    }
                    number=Integer.valueOf(number,2).toString();
                    out.write(number);
                }
            }
            out.flush();;
            out.close();;
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}