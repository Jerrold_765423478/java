# Java第二周作业

---

##本周作业：

1. 参考http://www.cnblogs.com/rocedu/p/7911138.html 学习第二三章视频
2. 参考http://www.cnblogs.com/rocedu/p/5155128.html，输入调试教材第二三章的代码，并把代码上传到码云上。注意commit message的写法。
3. 参考http://www.cnblogs.com/rocedu/p/6482354.html 提交脚本运行结果的截图。
4. 总结本周学习中遇到的问题和解决过程。

##学习结果：
1. 对课本二三章的代码进行了编译运行并学习了每个代码的功能。
码云链接：https://gitee.com/Jerrold_765423478/java/tree/master/src
![](https://images.gitee.com/uploads/images/2019/0310/182525_580803e3_1598615.png)
![](https://images.gitee.com/uploads/images/2019/0310/182525_fd3e1de7_1598615.png)
![](https://images.gitee.com/uploads/images/2019/0310/182525_a1ba3a8c_1598615.png)
![](https://images.gitee.com/uploads/images/2019/0310/182525_390dc1eb_1598615.png)
![](https://images.gitee.com/uploads/images/2019/0310/182525_d12748ae_1598615.png)
![](https://images.gitee.com/uploads/images/2019/0310/182526_35bc3a6a_1598615.png)
![](https://img2018.cnblogs.com/blog/1598514/201903/1598514-20190310170005413-727070997.png)
![](https://images.gitee.com/uploads/images/2019/0310/182526_68eb358a_1598615.png)
![](https://images.gitee.com/uploads/images/2019/0310/182526_08ded1e1_1598615.png)
![](https://images.gitee.com/uploads/images/2019/0310/182526_aa931cdc_1598615.png)
![](https://images.gitee.com/uploads/images/2019/0310/182526_2916fd0e_1598615.png)
![](https://images.gitee.com/uploads/images/2019/0310/182527_053d140d_1598615.png)
![](https://images.gitee.com/uploads/images/2019/0310/182527_872807ed_1598615.png)
![](https://images.gitee.com/uploads/images/2019/0310/182527_093de1ff_1598615.png)
![](https://images.gitee.com/uploads/images/2019/0310/182528_07d57348_1598615.png)
![](https://images.gitee.com/uploads/images/2019/0310/182528_f0e48d0d_1598615.png)

2.  完成了实验一的（1）（2）部分。

##过程中遇到的问题：
1.  我将老师在蓝墨云班课中提供的代码资源通过Windows的git上传到了码云上，在Ubuntu中使用`git clone`将代码克隆后在vim中显示乱码且无法编译。
![](https://images.gitee.com/uploads/images/2019/0310/182528_b604391f_1598615.png)
不明原因。后来使用了共享文件夹。
2.  在编译过程中始终觉得手打代码过于复杂，没有当时使用的C语言编译器中类似的自动补全功能。
我搜索了vim使用手册发现一些固定短语可以通过`ctrl+N`或`ctrl+P`补全，但是还是有很多语法不能补全。
搜索到了vim插件YouCompleteMe+Eclim以及JavaComplete有java语言补全功能。但是下载及配置还需要再尝试学习。
链接：
1.YCM：https://www.cnblogs.com/luhouxiang/p/5809133.html 
2.Javacomplete2：https://yanbin.blog/vim-java-autocomplete-with-vim-javacomplete2/#more-7708
3.  实验中涉及到的JDK，IEDA压缩包类型的下载，在下载过程中我使用了共享文件夹。使用Ubuntu解压时对于老师博客中`tar -xzvf ···`有疑惑，于是查找了Linux中`tar`命令的相关信息。
链接：https://www.cnblogs.com/newcaoguo/p/5896975.html
4.  在完成实验一（1）的过程中不会在主目录中运行bin目录中的class文件。
通过`java -h`得到了帮助，同学在蓝墨云中也给了解答。可以使用`java -cp <目录> <主类名>`实现。

##收获：
本周的学习令我对这种发现问题自己学习的方式更加适应习惯，也更加喜欢，同时也从中获得了很多知识和方法。