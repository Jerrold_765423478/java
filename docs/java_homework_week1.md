# Java第一周作业


---

##本周作业：
1. 参考<<教材学习指导(http://www.cnblogs.com/rocedu/p/7911138.html)） 学习第一章视频
2. 参考<<使用开源中国(码云)托管代码（https://www.cnblogs.com/rocedu/p/5155128.html）>>，<<使用JDB调试Java程序(https://www.cnblogs.com/rocedu/p/6371262.html)>> 输入调试教材上代码，并把代码上传到码云上。注意commit message的写法。
3. 参考http://www.cnblogs.com/rocedu/p/6482354.html 提交脚本运行结果的截图。
4. 总结本周学习中遇到的问题和解决过程。推荐使用博客园发表博客，优秀博客有加分。

##学习结果
1. 学习了Java的基本语言。
2. 学习使用git bush中的vim编写以及修改Java程序，并使用JDB调试程序。
3. 使用git将代码上传至我的[码云库](https://gitee.com/Jerrold_765423478/java)。
4. 将作业完成情况以及问题写成博客并发表在博客园中


##过程中遇到的问题

1. 在从C语言转换到Java语言过程中与些许不适应。
2. 习惯了C语言用编译器编写程序，使用git bush中的vim编写Java时有些许不适应。
3. 在起初使用vim时不会操作。
解决办法：在网上搜索了git中的[vim教程](https://www.cnblogs.com/hezhiyao/p/9418468.html)
4.  使用git时遇到打错密码之后无法更改的情况。
解决办法：在网上搜索了解决办法，使用`git config --system --unset credential.helper`可以清除自动保存的账户密码。
5.  上传代码后再码云上删除该代码，会出现再次上传失败的情况。
暂不清楚原因及解决办法。